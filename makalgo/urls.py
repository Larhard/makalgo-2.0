from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'makalgo.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^forum/', include('forum.urls', namespace='forum')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('accounts.urls', namespace='accounts')),
    url(r'^base/', include('base.urls', namespace='base')),
    url(r'^user_activity/', include('user_activity.urls', namespace='user_activity')),
    url(r'', include('forum.urls', namespace='forum')),
)
