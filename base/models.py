from django.db import models

# Create your models here.


class Config (models.Model):
    key = models.TextField(max_length=16, unique=True)
    value = models.TextField(max_length=128)

    def __unicode__(self):
        return u'{:16} : {}'.format(self.name, self.value)
