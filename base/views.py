from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from datetime import timedelta
from base.models import Config
import datetime
import time

# Create your views here.


def change_theme(request):
    response = HttpResponseRedirect(request.GET.get('next', '/'))
    theme = request.COOKIES.get('theme', 'default')
    if theme == 'default':
        theme = 'light'
    else:
        theme = 'default'
    response.set_cookie('theme', theme, max_age=365*24*60*60)
    return response


def update_revision(request):
    entry, created = Config.objects.get_or_create(key='revision')
    entry.value = str(time.mktime(datetime.datetime.now().timetuple()))
    entry.save()
    return HttpResponseRedirect(request.META['HTTP_REFERER'])