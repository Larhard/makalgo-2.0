from django.conf.urls import patterns, url

from base import views

urlpatterns = patterns('',
                       url (r'^change_theme$', views.change_theme, name='change_theme'),
                       url (r'^update_revision$', views.update_revision, name='update_revision'),
                       )