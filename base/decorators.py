from django.shortcuts import render
from django.http import Http404


def authentication_check(func):
    def wrapper(request, *args, **kwargs):
        if request.user.is_authenticated():
            return func(request, *args, **kwargs)
        return render(request, 'base/not_allowed.html')
    return wrapper


def post_check(func):
    def wrapper(request, *args, **kwargs):
        if request.method == 'POST':
            return func(request, *args, **kwargs)
        else:
            raise Http404
    return wrapper


def superuser_check(func):
    def wrapper(request, *args, **kwargs):
        if request.user.is_superuser:
            return func(request, *args, **kwargs)
        raise Http404
    return wrapper

