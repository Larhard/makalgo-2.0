from django import template
from makalgo.settings import STATIC_URL
from re import sub


register = template.Library()


def css_dir(context, css):
    dir = 'default'
    request = context['request']
    dir = request.COOKIES.get('theme', 'default')
    css = sub(r'css', 'css/%s' % (dir), css, 1)
    return STATIC_URL + css

register.simple_tag(css_dir, True)
