from django import template
from base.models import Config
from django.core.exceptions import ObjectDoesNotExist

register = template.Library()


def site_revision():
    try:
        return Config.objects.get(key='revision').value
    except ObjectDoesNotExist:
        return 0

register.simple_tag(site_revision)
