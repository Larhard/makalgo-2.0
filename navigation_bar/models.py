from django.db import models

# Create your models here.


class Navigation (models.Model):
    title = models.CharField(max_length=32)
    destination = models.CharField(max_length=32)
    position = models.IntegerField(default=1024)
