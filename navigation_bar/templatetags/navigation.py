from django.core.urlresolvers import reverse
from django import template
from django.template import resolve_variable
from django.utils.http import urlencode
from forum.views import get_sound_icon

register = template.Library()

NAVIGATION_BARS = {
    'main': [
        {'title_func': get_sound_icon, 'name': 'forum:toggle_sounds', 'is_authenticated': True, 'args': {'next': '\\path'}, },
        {'title': 'Change Theme', 'name': 'base:change_theme', 'is_authenticated': True, 'args': {'next': '\\path'}, },
        {'title': 'Login', 'name': 'accounts:login', 'is_authenticated': False, 'args': {'next': '\\path'}, },
        {'title': 'Logout', 'name': 'accounts:logout', 'is_authenticated': True, 'args': {'next': '\\path'}, },
        {'title': 'Refresh', 'name': 'base:update_revision', 'is_super': True}
    ]
}


def check(entry, request):
    if 'logged_in' in entry:
        if request.user.is_authenticated() != entry['logged_in']:
            return False
    return True


def show_navigation(context, name):
    user = context['user']
    request = context['request']
    response = []
    bar = NAVIGATION_BARS[name]
    for entry in bar:
        url = ''
        args = ''
        if 'is_authenticated' in entry and entry['is_authenticated'] != user.is_authenticated():
            continue
        if 'is_super' in entry and entry['is_super'] != user.is_superuser:
            continue
        if 'name' in entry:
            url = reverse(entry['name'])
        if 'url' in entry:
            url = entry['url']
        if 'args' in entry:
            argument = entry['args']
            args = '?'
            for key in argument:
                if argument[key] == '\\path':
                    argument[key] = request.path
            args = '?%s' % (urlencode(entry['args']))
        response.append({
            'title': entry['title_func'](context) if 'title_func' in entry else entry['title'],
            'url': url + args
        })

    return {'entries': response}

register.inclusion_tag('navigation_bar/navigation_list.html', takes_context=True)(show_navigation)
