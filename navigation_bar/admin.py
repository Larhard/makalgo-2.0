from django.contrib import admin

from navigation_bar.models import Navigation

# Register your models here.


class NavigationAdmin (admin.ModelAdmin):
    fields = [
        'title',
        'destination',
        'position',
    ]

    ordering = ('position', )
    list_display = ('title', 'destination', )


admin.site.register(Navigation, NavigationAdmin)
