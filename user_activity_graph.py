#!/usr/bin/env python
#-*- coding: utf-8 -*-
if __name__ == '__main__':
    import sys
    import os
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "makalgo.settings")
    from django.conf import settings
    #settings.DATABASES['default']['NAME'] = '/home/larhard.gentoo/makalgo-2.0/db.sqlite3'

from forum.models import Author, Visibility
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.dates as md
from django.utils import timezone
from datetime import timedelta
from itertools import cycle

from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.dates import DateFormatter
from django.http import HttpResponse


def user_graph(output=None, delta={'days': 2}):
    figure = plt.figure()

    good_time = timezone.now()-timedelta(**delta)
    authors = Author.objects.filter(last_seen__end__gte=good_time).order_by('username')
    ylabels = [k.username for k in authors]

    plt.yticks(np.arange(len(ylabels)), ylabels)

    plt.gcf().autofmt_xdate()

    colors = cycle('rgb')
    plt.ylim([-.5, len(authors) + .5])
    for (idx, author) in enumerate(authors):
        color = next(colors)
        for visibility in author.visibility_set.filter(end__gte=good_time):
            plt.plot([visibility.begin, visibility.end], [idx + 0.001, idx + 0.002], lw=3, c=color)
    if output == None:
        plt.show()
    else:
        canvas = FigureCanvas(figure)
        response = HttpResponse(content_type='image/png')
        canvas.print_png(response)
        return response


if __name__ == '__main__':
    user_graph()


