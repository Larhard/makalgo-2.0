
function update_post_list() {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url:update_url,
        data: {
            'last': last
        },
        success: function(data){
           if (data['revision'] > revision || data ['refresh']) {
                document.location.reload()
            }
            post_list = $('#post_list')
            row = ['row1', 'row2']
            messages = data['messages']
            var sound = false
            for (i in messages) {
                post_list.prepend ('<li class="post new animate '+row[idx % 2]+'"><div class="post_header"><div class="post_date">'+messages[i].date+'</div><div class="post_author">'+messages[i].author+'</div></div><div class="post_text">'+messages[i].text+'</div></li>')
                idx += 1
                animate = true
                sound = true
                document.title = "✉ " + base_title
            }
            if (sound && message_sound) {
                message_sound.play()
            }
            last = data['last']
            user_update = data['users']
            for (i in user_update) {
                if (! (i in users)) {
                    users[i] = user_update[i]
                    user_list.append('<li class="user'+i+'">'+user_update[i]+'</li>')
                }
            }
            for (i in users) {
                if (! (i in user_update)) {
                    delete users[i]
                    user_list.find('.user'+i).remove()
                }
            }
            if (conn_error) {
                alert_div.find('.conn').remove()
                conn_error = false
            }
        },
        error: function() {
            if (!conn_error) {
                alert_div.append('<li class="conn">connection lost</li>')
                conn_error = true
            }
        },
        complete: function () {
            setTimeout ('update_post_list()', 5000)
        }
    })
}

var animate = false
var idx = 1
var message_sound = false
var base_title = ""

$(document).ready(function() {
    users = {}
    user_list = $('#user_list')
    alert_div = $('#update_alert')
    conn_error = false
    base_title = document.title
    if (message_sound_path) {
        message_sound = new Audio(message_sound_path)
    }
    update_post_list()
    $('ul#user_list').mouseup(function(){
        $(this).stop(true, true).toggleClass('hidden', 500)
    })
    //setTimeout ('update_post_list()', 5000)
})

$(document).mousemove(function() {
    if (animate) {
        document.title = base_title
        $('.animate').stop().removeClass('animate').removeClass('new', 500)
    }
    animate = false
})
