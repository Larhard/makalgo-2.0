from django.conf.urls import patterns, url

from user_activity import views

urlpatterns = patterns('',
                       url(r'^(?P<days>\d+)/graph.png$', views.show_user_graph, name='graph'),
                       )
