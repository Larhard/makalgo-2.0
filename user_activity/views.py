from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.

from user_activity_graph import user_graph
from base.decorators import authentication_check, superuser_check


@authentication_check
def show_user_graph(request, days):
    return user_graph(True, delta={'days': int(days)})
