from django.conf.urls import patterns, url

from accounts import views

urlpatterns = patterns('',
                       url(r'^login$', views.login_form, name='login'),
                       url(r'^authenticate$', views.auth, name='authenticate'),
                       url(r'^logout$', views.deauth, name='logout'),
                       )
