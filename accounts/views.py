from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

# Create your views here.


def login_form(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(request.GET.get('next', '/'))
    return render(request, 'accounts/login.html', {'next': request.GET.get('next', '/')})


def auth(request):
    if 'password' in request.POST:
        user = authenticate(username='makalgo_user', password=request.POST['password'])
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect(request.POST.get('next', '/'))
    return render(request, 'accounts/login.html', {'next': request.POST.get('next', '/')})


def deauth(request):
    logout(request)
    return HttpResponseRedirect(request.GET.get('next', '/'))
