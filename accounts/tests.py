from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse

# Create your tests here.


class LoginTests (TestCase):
    def test_false_authenticate_view(self):
        self.user_name = 'makalgo_user'
        self.user_password = 'random324jki32@'
        User.objects.create_user(self.user_name, '', self.user_password)
        response = self.client.post(reverse('accounts:authenticate'), {'password': 'random_password'})
        self.assertEqual(response.status_code, 200)

    def test_authenticate_view(self):
        self.user_name = 'makalgo_user'
        self.user_password = 'random324jki32@'
        User.objects.create_user(self.user_name, '', self.user_password)
        response = self.client.post(reverse('accounts:authenticate'), {'password': self.user_password})
        self.assertEqual(response.status_code, 302)
