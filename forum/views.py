from django.shortcuts import render, get_object_or_404
from django.templatetags.static import static
from django.views import generic
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.utils import timezone
from base.decorators import authentication_check, post_check
import json
from django.utils.formats import date_format
from django.utils.timezone import localtime
from django.core.context_processors import csrf
from django.template.defaultfilters import linebreaksbr
from datetime import timedelta
from django.core.exceptions import ObjectDoesNotExist

from forum.models import Messages
from forum.models import Author, Visibility
from base.models import Config


def get_author(request):
    return request.COOKIES.get('author', 'Anonymous')


def get_message_sound(request):
    if int(request.COOKIES.get('play_sounds', '1')):
        return static('forum/media/message_sounds/default.wav')
    return None


@authentication_check
def message_list_view(request, error_message=None):
    author = get_author(request)
    data = {
        'messages': Messages.objects.order_by('-date')[:50],
        'author': author,
        'error_message': error_message,
        'message_sound': get_message_sound(request),
    }
    response = render(request, 'forum/message_list.html', data)
    return response


@authentication_check
def add_post(request):

    if 'post_content' in request.POST and 'post_author' in request.POST:
        if 'change_nick' not in request.POST:
            new_message = Messages(author=request.POST['post_author'], text=request.POST['post_content'], date=timezone.now())
            new_message.save()

        response = HttpResponseRedirect(reverse('forum:list_view'))
        response.set_cookie('author', request.POST.get('post_author', 'Anonymous').encode('utf-8'), max_age=365*24*60*60)

        return response

    return message_list_view(request, 'something gone wrong')


def user_list(admin=False):
    users = Author.objects.filter(last_seen__end__gte=timezone.now()).order_by('username')
    users = {'%d' % k.id: k.username for k in users if k.username[-1] != '_' or admin}
    return users


@post_check
@authentication_check
def update_posts(request):
    refresh = False
    last = int(request.POST.get('last', 0))
    username = get_author(request)

    (author, created) = Author.objects.get_or_create(username=username)

    if created or author.last_seen.end < timezone.now():
        vis = Visibility(begin=timezone.now(), end=timezone.now() + timedelta(seconds=30), author=author)
        vis.save()
        author.last_seen = vis
        author.save()
    else:
        author.last_seen.end = timezone.now() + timedelta(seconds=30)
        author.last_seen.save()

    try:
        last_message = Messages.objects.filter(id__lte=last).order_by('-date')[0]
        last_date = last_message.date
        #last = last_message.id
        messages = Messages.objects.filter(date__gt=last_date).order_by('date')
        last = messages.last().id
        messages = [{'author': k.author, 'text': linebreaksbr(k.text), 'date': date_format(localtime(k.date), 'DATETIME_FORMAT')} for k in messages]
    except (IndexError, AttributeError):
        messages = []
        pass

    try:
        revision = Config.objects.get(key='revision').value
    except ObjectDoesNotExist:
        revision = '0'

    j = json.dumps({'last': last, 'messages': messages, 'users': user_list(request.user.is_superuser), 'refresh': refresh, 'revision': revision})
    return HttpResponse(j, mimetype='application/json')


def get_sound_icon(context):
    return '<div class="sound_{}"></div>'.format(
        'on' if context['request'].COOKIES.get('play_sounds', '1') == '1' else 'off'
    )


def toggle_sounds(request):
    response = HttpResponseRedirect(request.GET.get('next', '/'))
    value = int(request.COOKIES.get('play_sounds', '1'))
    value = 1 - value
    response.set_cookie('play_sounds', str(value), max_age=365*24*60*60)
    return response
