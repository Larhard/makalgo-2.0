from django.conf.urls import patterns, url

from forum import views

urlpatterns = patterns('',
                       #url (r'^$', views.MessageList.as_view (), name='list_view'),
                       url (r'^$', views.message_list_view, name='list_view'),
                       url (r'^add_post$', views.add_post, name='add_post'),
                       url (r'^update$', views.update_posts, name='update_posts'),
                       url (r'^toggle_sounds$', views.toggle_sounds, name='toggle_sounds'),
                       )