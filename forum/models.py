from django.db import models
from django.utils import timezone

# Create your models here.


class Messages(models.Model):
    text = models.TextField(max_length=65536)
    date = models.DateTimeField('publishing date', default=timezone.now())
    author = models.CharField(max_length=30)

    def __unicode__(self):
        return u'%s' % self.text


class Visibility(models.Model):
    author = models.ForeignKey('Author', null=True, blank=True)
    begin = models.DateTimeField(default=timezone.now())
    end = models.DateTimeField(default=timezone.now())
    ip1 = models.GenericIPAddressField(default='0.0.0.0')
    ip2 = models.GenericIPAddressField(default='0.0.0.0')

    def __unicode__(self):
        return u': %s - %s' % (self.begin, self.end)


class Author(models.Model):
    username = models.CharField(max_length=30, unique=True)
    last_seen = models.ForeignKey(Visibility, related_name='+', null=True)

    def __unicode__(self):
        return u'%s' % self.username
