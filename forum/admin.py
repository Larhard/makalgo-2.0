from django.contrib import admin

from forum.models import Messages
from forum.models import Author, Visibility

# Register your models here.

class MessagesAdmin (admin.ModelAdmin):
    fields = [
        'date',
        'author',
        'text'
    ]

    def shortener (message):
        return message.text [:80]

    list_display = ('author', shortener, 'date', )

admin.site.register(Messages, MessagesAdmin)


class VisibilityAdmin (admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['author', 'begin', 'end',]}),
        ('Ip', {'fields': ['ip1', 'ip2', ], 'classes': ['collapse']}),
    ]


class VisibilityInline (admin.TabularInline):
    model = Visibility


class AuthorAdmin (admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['username', 'last_seen']}),
        #('Ip', {'fields': ['ip', 'ip2', ], 'classes': ['collapse']}),
    ]
    #inlines = [VisibilityInline]

admin.site.register(Author, AuthorAdmin)
admin.site.register(Visibility, VisibilityAdmin)
